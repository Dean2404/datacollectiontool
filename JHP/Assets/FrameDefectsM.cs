﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;

public class FrameDefectsM : MonoBehaviour
{

    public Text Result;

    public GameObject Pinning;
    public GameObject Textobjectpin;

    public GameObject Decor;
    public GameObject Textobjectdeco;


    private Text Decorate;
    private Text pintext;

    public Toggle[] toggles;


    void Start()
    {

        pintext = Textobjectpin.GetComponent<Text>();
        Decorate = Textobjectdeco.GetComponent<Text>();


    }

    public void SetText()
    {
        for (int i = 0; i < toggles.Length; i++)
        {
            if (toggles[i].isOn)
            {
                print(toggles[i].transform.GetChild(2).GetComponent<Text>().text);
            }
        }
    }

}

