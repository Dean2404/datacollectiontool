﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinState : MonoBehaviour {

    //script functionality- Enum state incase you want to be able to toggle out when you can place pins or just view them 
    public enum Pinstate
    {
        View,
        Place
    }

    public static Pinstate pinState;

}
