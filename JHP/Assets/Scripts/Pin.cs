﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Pin class instance that can be added to a pin to give it further details, such as a name, position, maybe a subsection etc.

[SerializeField]
public class Pin: MonoBehaviour{

    //name that you give to the pin, this is used to loop through all the pins and grab individual references
    public string pinName;

    //the position of the pin, not yet used but could be useful for database to hold
    public Vector2 pinPos;

    //the string name that holds the current selected dropdown
    public string currentDropdown;

    //constructor/initialiser function called when a pin is instantiated to set default values
    public void Data(string name, Vector2 pos, string currentDropdown)
    {
        this.pinName = name;
        this.pinPos = pos;
        this.currentDropdown = currentDropdown;
    }
}
