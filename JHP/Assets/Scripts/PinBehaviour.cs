﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PinBehaviour : MonoBehaviour, ISelectHandler, IDragHandler, IEndDragHandler
{
    //script functionality - is contained on each pin that is spawned so it can be selected and send the correct data 

    //bool to decide between whether we're dragging or just clicking 
    bool isDragging;

    //this pins class reference
    private Pin pinData;

    private void Start()
    {
        //this pins class initialisation
        pinData = GetComponent<Pin>();
    }

    //called when dragging so we dont confuse dragging with a click to select or place
    public void OnDrag(PointerEventData eventData)
    {
        isDragging = true;
    }

    //called when dragging so we dont confuse dragging with a click to select or place
    public void OnEndDrag(PointerEventData eventData)
    {
        isDragging = false;
    }

    //function called when a pin is selected
    public void OnSelect(BaseEventData eventData)
    {
        if (!isDragging)
        {
            // sets the pin info panel to active
            PinUI.Instance.pinPanel.SetActive(true);

            //assigns the current selected pin as this pin
            PinUI.Instance.GetCurrentPin(this.pinData.pinName);
            PinUI.Instance.AssignName();
            //null or empty string check as the pin was initialised with this field empty, if it snot empty grab its info
            if (!string.IsNullOrEmpty(pinData.currentDropdown))
            {
                PinUI.Instance.GetSelectedInfo();
            }

            //if it is empty we want to reset the info and assign the pin the current selected item 
            else
            {
                PinUI.Instance.SaveChanges();
                PinUI.Instance.ResetDropdown();
            }               
        }
    }


}
