﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class DataLoader : MonoBehaviour {

    public string[] items;
    int indexer;

    public GameObject panelPrefab;
    public GameObject floorPrefab;
    public GameObject hospitalPanel;
    public GameObject floorPanel;

    public Texture2D testPic;
    public RawImage loadIn;


    public Button BackButton;

    private string HospitalList = "http://martinkorodi.000webhostapp.com/HospitalLister.php";
    private string FloorList = "http://martinkorodi.000webhostapp.com/FloorList.php";
    private string imageUpload = "http://martinkorodi.000webhostapp.com/ImageUpload.php";
    private string imageDownload = "https://martinkorodi.000webhostapp.com/Images/wenamelikethis";

    private void Start()
    {
       // loadIn.GetComponent<Texture2D>();
        StartCoroutine(HospitalLister());
     //   StartCoroutine(ImgTest(testPic));
        StartCoroutine(ImgLoad(loadIn));
    }

    //public void ShowMe()
    //{
    //    StartCoroutine(HospitalLister());
    //}

    private IEnumerator ImgTest(Texture2D text)
    {
        byte[] bytes = text.EncodeToPNG();

        WWWForm ImageUpload = new WWWForm();
        ImageUpload.AddBinaryData("imageFromUnity", bytes, "wenamelikethis", "image/png");

        WWW imgupload = new WWW(imageUpload, ImageUpload);

        print("wecomehere");

        yield return imgupload;
        string itemsDataString = imgupload.text;
        print(itemsDataString);

    }

    private IEnumerator ImgLoad(RawImage text)
    {
        WWW imageLink = new WWW(imageDownload);
        yield return imageLink;

        text.texture = imageLink.texture;
        text.SetNativeSize();

    }



    private IEnumerator HospitalLister ()
    {
        WWW hospitalData = new WWW(HospitalList);
        yield return hospitalData;

        string itemsDataString = hospitalData.text;
        print(itemsDataString);

        items = itemsDataString.Split(';');

        indexer = 0;

        while (indexer < items.Length - 1)
        {
            int hospitalID = int.Parse(GetDataValue(items[indexer], "ID:"));
            string hospitalName = GetDataValue(items[indexer], "Name:");
            GameObject objInst;

            objInst = Instantiate(panelPrefab, hospitalPanel.transform);
            objInst.transform.GetChild(0).GetComponent<Text>().text = hospitalName;
            objInst.transform.GetChild(1).GetComponent<Text>().text = GetDataValue(items[indexer], "Location:");
            objInst.transform.GetChild(2).GetComponent<Button>().onClick.AddListener(delegate { ButtonInstantiate(hospitalID, hospitalName); });

            print(GetDataValue(items[indexer], "Location:"));
            indexer++;
        }
       
        
	}

    public void ButtonInstantiate(int hospitalNum, string hospitalNam)
    {
        StartCoroutine(GetFloors(hospitalNum, hospitalNam));
    }

    private IEnumerator GetFloors(int hospitalID, string hospitalName)
    {
        WWWForm ListFloors = new WWWForm();
        ListFloors.AddField("hospID", hospitalID);

        WWW upload = new WWW(FloorList, ListFloors);

        yield return upload;

        string itemsDataString = upload.text;
        print(itemsDataString);

        items = itemsDataString.Split(';');

        hospitalPanel.SetActive(false);
        floorPanel.SetActive(true);
        BackButton.gameObject.SetActive(true);

        indexer = 0;

        while (indexer < items.Length - 1)
        {
            GameObject objInst;

            objInst = Instantiate(floorPrefab, floorPanel.transform);
            objInst.transform.GetChild(0).GetComponent<Text>().text = "Floor" + GetDataValue(items[indexer], "FloorNumber:");
            objInst.transform.GetChild(1).GetComponent<Text>().text = hospitalName;
            //  objInst.transform.GetChild(2).GetComponent<Button>().onClick.AddListener(delegate { ButtonInstantiate(hospitalID); });

            indexer++;

        }
    }


    private string GetDataValue(string data, string index)
    {
        string value = data.Substring(data.IndexOf(index) + index.Length);
        if (value.Contains("|"))
        {
            value = value.Remove(value.IndexOf("|"));
        }
       
        return value;
    }

    public void BackButtonDo()
    {
        foreach (Transform child in floorPanel.transform)
        {
            GameObject.Destroy(child.gameObject);
        }

        foreach (Transform child in hospitalPanel.transform)
        {
            GameObject.Destroy(child.gameObject);
        }

        floorPanel.SetActive(false);
        BackButton.gameObject.SetActive(false);
        hospitalPanel.SetActive(true);
        StartCoroutine(HospitalLister());
    }
	
}
