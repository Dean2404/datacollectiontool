﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PinUI : MonoBehaviour
{
    //script functionality that controls all the UI of the pins

    #region Variables

    //panel info for a pin when it is selected 
    public GameObject pinPanel;

    //dropdown that the user can choose options from 
    public Dropdown dropdown;

    //singleton pattern for this script so each Pin can access it easily 
    public static PinUI Instance;

    //reference to the pin manager that holds all the references for the pins
    public PinManager pinManager;

    //current selected pin instance
    public Pin currentPin;

    //text field to change out the pin name 
    public Text pinName;

    #endregion

    //assign singleton instance and set panel to inactive 
    private void Awake()
    {
        Instance = this;
        pinPanel.SetActive(false);

        //This is an enum state incase you want to be able to toggle to add points or not
        //PinState.pinState = PinState.Pinstate.View;
    }

    //function that assigns the corresponding dropdown based on the pins dropdown string 
    public void AssignDropdown(string value)
    {
        int dropdownIndex = 0;

        for (int i = 0; i < dropdown.options.Count; i++)
        {
            if (dropdown.options[i].text == value)
            {
                dropdownIndex = i;
            }
        }
        print(dropdown.value + " " + dropdownIndex);
        dropdown.value = dropdownIndex;
    }

    //assigns the text to be the pins name
    public void AssignName()
    {
        pinName.text = currentPin.pinName;
    }

    //resets the dropdown when a new pin is spawned
    public void ResetDropdown()
    {
        int dropdownIndex = 0;
        dropdown.value = dropdownIndex;
    }

    //this is used to toggle the enum state incase you want to toggle in and out of being able to place pins
    public void TogglePinState()
    {
        if (PinState.pinState == PinState.Pinstate.Place)
        {
            PinState.pinState = PinState.Pinstate.View;
        }
        else
        {
            PinState.pinState = PinState.Pinstate.Place;
        }
    }

    //closes the ui panel and sets current pin to inactive
    public void Close()
    {
        pinPanel.SetActive(false);
        currentPin = null;
    }

    //function that calls corresponding UI updates
    public void GetSelectedInfo()
    {
        AssignDropdown(currentPin.currentDropdown);
        AssignName();
    }

    //function that saves the dropdown changes on the current selected pin 
    public void SaveChanges()
    {
        currentPin.currentDropdown = PinUI.Instance.dropdown.options[PinUI.Instance.dropdown.value].text;
    }

    //function that grabs a reference to the currently selected pin, has to loop through all potential pins 
    public void GetCurrentPin(string name)
    {
        //loops through all pins 
        foreach (Pin pin in pinManager.pins)
        {
            //if the pin matches the pin we're looking for assign it and break out of loop as pin has already been found 
            if (pin.pinName == name)
            {
                currentPin = pin;
                break;
            }
            else
            {
                //reset pin if no corresponding pin was found 
                currentPin = null;
            }
        }
    }


}
